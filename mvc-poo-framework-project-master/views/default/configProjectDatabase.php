<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 17/06/18
 * Time: 17:41
 */
?>
        <h1 class="display-4" id="connecter">Configuration des paramètres d'accès à la base de données du projet</h1>
        <section id="formConnect">
            <form id="connexion"   action="../../../create/database/access/" method="POST">
                <p><label>Driver : </label>
                    <input name="driver" type="text" placeholder="mysql" id="driver" value="mysql"/></p>
                <!--        <input name="id" type="text" placeholder="id" id="id"/></p>-->

                <p><label>Port : </label>
                    <input type="text" name="port" placeholder="port" id="mdp" value="3306"/></p>

                <p><label>Host : </label>
                    <input type="text" name="host" placeholder="host" id="host" value="localhost" /></p>

                <p><label>Nom d'utilisateur : </label>
                    <input type="text" name="username" placeholder="username" id="username" value="root"/></p>

                <p><label>Mot de passe : </label>
                    <input type="text" name="password" placeholder="password" id="password" value=""/></p>

                <p><label>Nom de la base de données : </label>
                    <input type="text" name="dbname" placeholder="dbname" id="dbname" value=""/></p>

                <button type="submit" class="btn btn-primary mb-2">Valider</button>


            </form>
        </section>
