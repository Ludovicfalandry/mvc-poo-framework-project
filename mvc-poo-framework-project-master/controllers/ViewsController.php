<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 29/05/18
 * Time: 15:55
 */

namespace Controllers;

use Core\Controller;
use Core\Response;

class ViewsController extends Controller
{

    public function getHome(){
//        vardump($this);
       $this->render("accueil");
//        $viou = new Response("accueil.php");
//        $viou->setTitle( "Page d'accueil");
//        $viou->Render();
    }

    public function error404(){
        $view = new Response("error404.php");
        $view->setTitle( "Page d'erreur 404");
        $view->Render();
    }

    public function configProjectDatabase(){
        $view = new Response("configProjectDatabase.php");
        $view->setTitle( "Page de saisie des paramètres de configuration de la base de données");
        $view->Render();
    }

}