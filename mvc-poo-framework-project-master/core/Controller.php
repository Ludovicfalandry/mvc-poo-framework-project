<?php
/**
 * Created by PhpStorm.
 * User: padbrain
 * Date: 28/05/18
 * Time: 11:29
 */

namespace Core;

use SecurityMiddleware;
use RenderException;

/**
 *  La class abstraite Controller sera étendue par les différentes classes
 *  controlleur métier.
 *  Elle leur fourni un accès aux variables globales $_POST, $_GET et $_SERVER['REQUEST_URI]
 *  ainsi qu'aux paramètres manipulés à travers l'URI
 */

abstract class Controller
{

    /**
     * @var array
     */
    private $get;
    /**
     * @var array
     */
    private $post;
    /**
     * @var string
     */
    private $uri;

    /**
     *
     * Cette propriété sera initialisée lors de l'appel de la methode
     * securityLoader()
     * @var SecurityMiddleware
     */
    protected $security;

    /**
     * Initialise le contexte de security.
     */
    protected function securityLoader() {
        $this->security = new SecurityMiddleware();
    }

    /**
     * Controller constructor. Initialise les attributs devant contenir les
     * variables d'environnement utiles aux traitements
     * @param array|null $get
     * @param array|null $post
     * @param string|null $uri
     */
    public function __construct(array $get = null, array $post = null, string $uri = null)
    {
        $this->get = $get;
        $this->post = $post;
        $this->uri = $uri;
    }



    /**
     * La methode render affiche la vue selectionnée grace au premier argument
     * La methode utilise les indirections pour generer dynamiquement les noms des variables
     * utilisées dans les vues.
     *
     * @param string $pathToView chemin du fichier de vue demandé
     * @param array $datas La valeur par defaut permet de retourner des vues statiques
     */
    final protected function render($pathToView,$datas=null) {
        if(is_array($datas)){
            foreach ($datas as $key => $value) {
                $$key = $value;
            }
        }

        $view = new Response($pathToView . ".php");
        $view->setTitle("Modification d'un profil utilisateur");
        $view->Render();
    }

    /**
     * Le but de cette méthode est de transformer le contenu de l'URI
     *  stockée sous forme de chaîne de caractères ($_server['REQUEST_URI'])
     * en un tableau listant les paramètres contenus dans l'URI
     * @return array
     */
    protected function getParamFromUri() : array{
        //  RÉCUPÉRATION DE L'URI DANS UN TABLEAU
        $uri = explode("/", rtrim($this->uri, "/"));

        //  $aParams EST LA RÉPONSE RETOUNÉE EN FIN DE TRAITEMENT
        $aParams = [];

        //  Chaque élément de l'URI est traité
        foreach ($uri as $value){
            //  et récupéré dans un tableau sous forme de
            if((int)$value > 0){
                //  valeur
                if(isset($key)) $aParams[strtolower($key)] = $value;
            }else{
                //  ou de clef
                $key = $value;
            }
        }
        //  Le tableau de paramètres est retourné
        return $aParams;
    }

    /**
     * retourne la propriété $post afin de la rendre disponible aux developpeurs
     * souhaitant étendre cette classe
     *
     * @return array
     */
    protected function inputPost() {
        return $this->post;
    }

    /**
     * retourne la propriété $get afin de la rendre disponible aux developpeurs
     * souhaitant étendre cette classe
     *
     * @return array
     */
    protected function inputGet() {
        return $this->get;
    }


}